$(document).ready(function()
{
	myMap.initialize();
});

var myMap = 
{
  map : null,
  geocoder: null,
	markers : [],
	infowindow  : null,
	minPrice : 0,
	maxPrice : 0,
	districtList : [],
	transaction : [
	  {
	    name : '臺中市西區梅川西路二段1~30號',
	    address : 'Times Square, New York, NY',
	    lat : '40.759058',
	    lng : '-73.985185',
	    price : 10000000,
	  },
	  {
	    name : '臺中市西區梅川西路二段1~30號',
	    address : 'New York University, New York, NY',
	    lat : '40.748660',
	    lng : '-73.985718',
	    price : 20000000,
	  },
	],
	
	initialize : function()
	{
		this.createMap();
	  this.geocodeAddress();
		this.setMarkersAndInfoWindow();
		this.setMinMaxPrices();
		this.displayTransactions();
		this.setSpinner();
		this.setSlider();
		this.setCounter();
	},
	
  createMap : function()
	{
	  var pinLocation = new google.maps.LatLng(40.759106, -73.985142);
    var myOptions = {
        zoom: 15,
        center: pinLocation,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // Allow panning across the map
        panControl: false,
        // Set the zoom level of the map
        zoomControl: true,
        zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
        position: google.maps.ControlPosition.TOP_RIGHT
        },
        // Switch map types
        mapTypeControl: true,
        mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_CENTER
        },
        // Show the scale of the map
        scaleControl: true,
        scaleControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        // A Pegman icon that can be dragged and dropped onto the map to show a street view
        streetViewControl: false,
        // A thumbnail showing a larger area, that reflects where the current map is within that wider area 
        overviewMapControl: false,
    };
    this.map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
	},
	
  geocodeAddress : function(){
  	this.geocoder = new google.maps.Geocoder();
    for (var i = 0; i < this.transaction.length; i++) {
    	var $transaction = this.transaction[i];
      this.geocoder.geocode({'address': $transaction.address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          $transaction.lat = results[0].geometry.location.lat();
          $transaction.lng = results[0].geometry.location.lng();
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }
  },
  
	setMarkersAndInfoWindow : function(){
	  for(var i = 0; i < this.transaction.length; i++)
		{
			var $transaction = this.transaction[i];
			var $marker = new google.maps.Marker({
					position: new google.maps.LatLng($transaction.lat, $transaction.lng),
					map: this.map,
					title: $transaction.name,
					icon: "http://maps.gstatic.com/mapfiles/markers2/marker.png"
			});
			this.markers.push($marker);
		}
	},
	
	setMinMaxPrices : function()
	{
	  this.minPrice = this.transaction[0].price;
		this.maxPrice = this.transaction[0].price;
		for(var i = 0; i< this.transaction.length; i++)
		{
			this.minPrice = this.transaction[i].price < this.minPrice ? this.transaction[i].price : this.minPrice;
			this.maxPrice = this.transaction[i].price > this.maxPrice ? this.transaction[i].price : this.maxPrice;
		}
		$('#currentRange').text('NTD'+ this.minPrice + ' - ' + 'NTD ' + this.maxPrice);
	},
	
	displayTransactions : function()
	{
		var str = '';
		for (var i = 0; i < this.transaction.length; i++) {
			var $transaction = this.transaction[i];
			str += '<h3 data-price="' + $transaction.price + '">' + $transaction.name + '</h3>';
			str += '<div>';
			str += '<div class="ui-state-highlight ui-corner-all" style="padding: 5px;">Price: NTD ' + $transaction.price + '</div>';
			str += '</div>';
		}
		$('#listing').html(str);
		$('#listing').accordion(
		{
			collapsible: true,
			active : false,
			heightStyle : 'content'
		});
	},
	
	setSpinner : function()
	{
		$('#spinner').spinner(
		{
			min : 0,
			max : 18,
			stop : function( event, ui )
			{
				myMap.map.setZoom( parseInt( $(this).val(), 10 ));
			}
		});
	},
	
	setSlider : function()
	{
		$('#slider').slider(
		{
			min : myMap.minPrice,
			max : myMap.maxPrice,
			range : true,
			values : [myMap.minPrice, myMap.maxPrice],
			step : 100,
			slide : function(event, ui)
			{
				$('#currentRange').text('NTD '+ ui.values[0] + ' - ' + 'NTD ' + ui.values[1]);
				
			},
			stop : function(event, ui)
			{
				$('#listing h3').each(function()
				{
					var price = parseInt($(this).data('price'), 10);
					//headerIndex corresponds to 0 based index of transactions in object as well as in DOM
					var headerIndex = $('#listing h3').index($(this));
					if(price >= ui.values[0] && price <=ui.values[1])
					{
						$('#listing h3:eq('+headerIndex+')').show();
						myMap.markers[headerIndex].setMap(myMap.map);
					}
					else
					{
						$('#listing h3:eq('+headerIndex+')').hide();
						$('#listing div.ui-accordion-content:eq('+headerIndex+')').hide();
						myMap.markers[headerIndex].setMap(null);
					}
				});
			},
		});
	},

	setCounter : function()
	{
		for(var i = 0; i< this.districtList.length; i++){
			for(var j = 0; j< this.transaction.length; j++){
			  if (this.districtList[i].dis == this.hotelsList[j].district) this.districtList[i].num++;
			}
		}
	  var newContent = '';
		for (var k = 0; k < this.districtList.length; k++)
		{
			newContent += '<div class="subCounting">';
			newContent += this.districtList[k].dis + ' : ' + this.districtList[k].num;
			newContent += '</div>';
		}
		document.getElementById('counting').innerHTML = newContent;
	},
};
