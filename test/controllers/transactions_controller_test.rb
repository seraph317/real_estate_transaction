require 'test_helper'

class TransactionsControllerTest < ActionController::TestCase
  setup do
    @transaction = transactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transaction" do
    assert_difference('Transaction.count') do
      post :create, transaction: { address: @transaction.address, area: @transaction.area, barrier: @transaction.barrier, built_date: @transaction.built_date, city_usage: @transaction.city_usage, comment: @transaction.comment, district: @transaction.district, floor: @transaction.floor, id: @transaction.id, management: @transaction.management, material: @transaction.material, noncity_control: @transaction.noncity_control, noncity_usage: @transaction.noncity_usage, num_bath: @transaction.num_bath, num_hall: @transaction.num_hall, num_room: @transaction.num_room, num_transaction_build: @transaction.num_transaction_build, num_transaction_land: @transaction.num_transaction_land, num_transaction_park: @transaction.num_transaction_park, park_price: @transaction.park_price, park_type: @transaction.park_type, price: @transaction.price, price_per_area: @transaction.price_per_area, target: @transaction.target, total_floors: @transaction.total_floors, transaction_build_area: @transaction.transaction_build_area, transaction_date: @transaction.transaction_date, transaction_park_area: @transaction.transaction_park_area, usage: @transaction.usage }
    end

    assert_redirected_to transaction_path(assigns(:transaction))
  end

  test "should show transaction" do
    get :show, id: @transaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transaction
    assert_response :success
  end

  test "should update transaction" do
    patch :update, id: @transaction, transaction: { address: @transaction.address, area: @transaction.area, barrier: @transaction.barrier, built_date: @transaction.built_date, city_usage: @transaction.city_usage, comment: @transaction.comment, district: @transaction.district, floor: @transaction.floor, id: @transaction.id, management: @transaction.management, material: @transaction.material, noncity_control: @transaction.noncity_control, noncity_usage: @transaction.noncity_usage, num_bath: @transaction.num_bath, num_hall: @transaction.num_hall, num_room: @transaction.num_room, num_transaction_build: @transaction.num_transaction_build, num_transaction_land: @transaction.num_transaction_land, num_transaction_park: @transaction.num_transaction_park, park_price: @transaction.park_price, park_type: @transaction.park_type, price: @transaction.price, price_per_area: @transaction.price_per_area, target: @transaction.target, total_floors: @transaction.total_floors, transaction_build_area: @transaction.transaction_build_area, transaction_date: @transaction.transaction_date, transaction_park_area: @transaction.transaction_park_area, usage: @transaction.usage }
    assert_redirected_to transaction_path(assigns(:transaction))
  end

  test "should destroy transaction" do
    assert_difference('Transaction.count', -1) do
      delete :destroy, id: @transaction
    end

    assert_redirected_to transactions_path
  end
end
