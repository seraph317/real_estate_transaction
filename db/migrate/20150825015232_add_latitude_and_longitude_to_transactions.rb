class AddLatitudeAndLongitudeToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :lat, :float
    add_column :transactions, :lng, :float
  end
end
