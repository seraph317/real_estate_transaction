class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :district
      t.string :target
      t.string :address
      t.decimal :area
      t.string :city_usage
      t.string :noncity_usage
      t.string :noncity_control
      t.date :transaction_date
      t.integer :num_transaction_land
      t.integer :num_transaction_build
      t.integer :num_transaction_park
      t.integer :floor
      t.integer :total_floors
      t.string :build_type 
      t.string :usage
      t.string :material
      t.date :built_date
      t.decimal :transaction_build_area
      t.integer :num_room
      t.integer :num_hall
      t.integer :num_bath
      t.boolean :barrier
      t.boolean :management
      t.decimal :price
      t.decimal :price_per_area
      t.string :park_type
      t.decimal :transaction_park_area
      t.decimal :park_price
      t.text :comment
      t.string :transaction_id

      t.timestamps null: false
    end
  end
end
